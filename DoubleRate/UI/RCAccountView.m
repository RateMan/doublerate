//
//  RCAccountView.m
//  Double Rate
//
//  Created by Dmitry on 12.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCAccountView.h"
#import "RCCurrencyAccount.h"
#import "RCCurrencyUtils.h"
#import "NSDecimalNumber+RCUtils.h"
#import "RCDecimalNumberBehaviors.h"


static const CGFloat kBalanceFontSize = 14.;
static const CGFloat kNameFontSize = 32.;

static const CGFloat kHintFontSize = 22.;
static const CGFloat kHintColor = 0.8;
static const CGFloat kTextColor = 0.22;

static const CGFloat kNameLabelTop = 30.;
static const CGFloat kBalanceLabelTop = 20;

static const CGFloat kLeftItemsPadding = 20.;
static const CGFloat kRightItemsPadding = 20.;

static const CGFloat kMinTextFieldFontSize = 20.;
static const CGFloat kTextFieldTop = 30.;
static const CGFloat kTextFieldLeftPading = 30.;

const unichar kOutcomePrefixSign = '-';

@interface RCAccountView () <UITextFieldDelegate>
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *balanceLabel;
@property (nonatomic, strong) UILabel *incomingLabel;
@property (nonatomic, strong) UITextField *textField;

@property (nonatomic, strong) NSNumberFormatter *formatter;
@end

@implementation RCAccountView

#pragma mark - Memory Management and Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Build formatter for parsing user input
        self.formatter = [[NSNumberFormatter alloc] init];
        self.formatter.numberStyle = NSNumberFormatterDecimalStyle;
       
        [self buildViewHierarchy];
    }
    return self;
}

#pragma mark - Public Interface

- (void)setIncomeAmount:(NSDecimalNumber *)incomeAmount {
    NSAssert(incomeAmount != nil, @"");
    
    _incomeAmount = incomeAmount;
    _outcomeAmount = nil;
    
    // setup income stuff
    if ([_incomeAmount rc_isZero]) {
        self.incomingLabel.text = @"";
    } else {
        RCBalanceRoundBehaviour *roundBehaviour = [RCBalanceRoundBehaviour new];
        NSDecimalNumber *roundedIncome = [incomeAmount decimalNumberByRoundingAccordingToBehavior:roundBehaviour];
        self.incomingLabel.text = [NSString stringWithFormat:@"+%@", [roundedIncome descriptionWithLocale:[NSLocale currentLocale]]];
    }

    // reset outcome stuff
    
    self.balanceLabel.textColor = [self textColor];
    self.textField.text = @"";
    
    if (self.incomingLabel.text.length > 0) {
        self.textField.attributedPlaceholder = nil;
    } else {
        self.textField.attributedPlaceholder = [self buildOutcomePlaceholder];
    }
}

- (void)setOutcomeAmount:(NSDecimalNumber *)outcomeAmount {
    NSAssert(outcomeAmount != nil, @"");
    
    [self processOutcomeAmount:outcomeAmount];
    
    // setup income stuff
    if ([_outcomeAmount rc_isZero]) {
        self.textField.text = @"";
    } else {
        NSString *prefix = [NSString stringWithCharacters:&kOutcomePrefixSign length:1];
        self.textField.text = [prefix stringByAppendingString:
                               [outcomeAmount descriptionWithLocale:[NSLocale currentLocale]]];
    }
}

- (void)setAccount:(RCCurrencyAccount *)account {
    NSAssert(account != nil, @"");
    _account = account;
    [self applyData];
    [self setNeedsLayout];
}

- (BOOL)isFirstResponder {
    return [self.textField isFirstResponder];
}

- (BOOL)becomeFirstResponder {
    return [self.textField becomeFirstResponder];
}

#pragma mark - Inner Logic

- (void)applyData {
    self.nameLabel.text = self.account.currencyCode;
    self.balanceLabel.text = [self buildBalanceString];
}

- (NSString *)buildBalanceString {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *sign = signForCurrency(self.account.currencyCode);
    NSString *amount = [self.account.balance descriptionWithLocale:locale];
    
    return [NSString stringWithFormat:@"%@: %@%@", NSLocalizedString(@"BALANCE", nil), sign, amount];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize selfSize = self.bounds.size;
    
    // Layout name label
    [self.nameLabel sizeToFit];
    CGRect nameFrame = self.nameLabel.frame;
    nameFrame.origin = CGPointMake(kLeftItemsPadding, kNameLabelTop);
    self.nameLabel.frame = nameFrame;
    
    // Layout balance label
    [self.balanceLabel sizeToFit];
    CGRect balanceFrame = self.balanceLabel.frame;
    balanceFrame.origin = CGPointMake(kLeftItemsPadding, CGRectGetMaxY(nameFrame) + kBalanceLabelTop);
    balanceFrame.size.width = selfSize.width - kLeftItemsPadding - kRightItemsPadding;
    self.balanceLabel.frame = balanceFrame;
    
    // Layout text field
    CGRect fieldRect = CGRectZero;
    fieldRect.origin.x = CGRectGetMaxX(self.nameLabel.frame) + kTextFieldLeftPading;
    fieldRect.origin.y = kTextFieldTop;
    fieldRect.size.height = nameFrame.size.height;
    fieldRect.size.width = selfSize.width - fieldRect.origin.x - kRightItemsPadding;
    self.textField.frame = fieldRect;
    
    // Layout income label
    self.incomingLabel.frame = fieldRect;
}

- (void)validateBalance {
    BOOL validationError = self.outcomeAmount && ![self.account.balance rc_greaterOrEqualThan:self.outcomeAmount];
    self.balanceLabel.textColor = validationError ? [UIColor redColor] : [self textColor];
}

- (void)processOutcomeAmount:(NSDecimalNumber *)outcomeAmount {
    NSAssert(outcomeAmount != nil, @"");
    
    _outcomeAmount = outcomeAmount;
    _incomeAmount = nil;
    
    if (self.textField.attributedPlaceholder == nil) {
        self.textField.attributedPlaceholder = [self buildOutcomePlaceholder];
    }
    
    [self validateBalance];
    
    // reset income stuff
    self.incomingLabel.text = @"";
}

- (void)notifyOutcomeAmountChanged {
    if ([self.delegate respondsToSelector:@selector(accountViewDidChangeOutcomeAmount:)]) {
        [self.delegate accountViewDidChangeOutcomeAmount:self];
    }
}

- (void)notifyDidStartEditOutcome {
    if ([self.delegate respondsToSelector:@selector(accountViewDidStartEditOutcomeAmount:)]) {
        [self.delegate accountViewDidStartEditOutcomeAmount:self];
    }
}

#pragma mark - View Hierarchy

- (void)buildViewHierarchy {
    [self buildNameLabel];
    [self buildBalanceLabel];
    [self buildIncomingLabel];
    [self buildTextField];
}

- (void)buildNameLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [self textColor];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:kNameFontSize];
    [self addSubview:label];
    self.nameLabel = label;
}

- (void)buildBalanceLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [self textColor];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:kBalanceFontSize];
    [self addSubview:label];
    self.balanceLabel = label;
}

- (void)buildIncomingLabel {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.textColor = [self textColor];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:kNameFontSize];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.2;
    label.textAlignment = NSTextAlignmentRight;
    label.userInteractionEnabled = NO;
    [self addSubview:label];
    self.incomingLabel = label;
}

- (void)buildTextField {
    UITextField *text = [[UITextField alloc] initWithFrame:CGRectZero];
    text.font = [UIFont systemFontOfSize:kNameFontSize];
    text.adjustsFontSizeToFitWidth = YES;
    text.minimumFontSize = kMinTextFieldFontSize;
    text.textColor = [self textColor];
    text.keyboardType = UIKeyboardTypeDecimalPad;
    text.autocorrectionType = UITextAutocorrectionTypeNo;
    text.textAlignment = NSTextAlignmentRight;
    text.attributedPlaceholder = [self buildOutcomePlaceholder];
    [text addTarget:self action:@selector(onTextDidChange) forControlEvents:UIControlEventEditingChanged];
    text.delegate = self;
    [self addSubview:text];
    self.textField = text;
}

- (NSAttributedString *)buildOutcomePlaceholder {
    NSDictionary *attrs = @{NSFontAttributeName : [UIFont italicSystemFontOfSize:kHintFontSize],
                            NSForegroundColorAttributeName : [UIColor colorWithWhite:kHintColor alpha:1]};
    
    NSAttributedString *text = [[NSAttributedString alloc] initWithString:
                                NSLocalizedString(@"TYPE_THE_AMOUNT", nil) attributes:attrs];
    return text;
}

- (UIColor *)textColor {
    return [UIColor colorWithWhite:kTextColor alpha:1];
}

#pragma mark - User Actions

- (void)onTextDidChange {
    //  First validate input
    NSString *trimmedText = self.textField.text;
    if (self.textField.text.length > 0 &&
        [self.textField.text characterAtIndex:0] == kOutcomePrefixSign ) {
        trimmedText = [self.textField.text substringFromIndex:1];
    }
    
    NSNumber *inputNumber = [self.formatter numberFromString:trimmedText];
    BOOL inputError = (inputNumber == nil) && (trimmedText.length > 0);
    
    self.textField.textColor = inputError ? [UIColor redColor] : [self textColor];
    
    NSDecimalNumber *enteredAmount;
    if (inputError) {
        enteredAmount = [NSDecimalNumber zero];
    } else {
        
        // Build decimal number
        NSDecimalNumber *decimalAmount;
        if (trimmedText.length == 0) {
            decimalAmount = [NSDecimalNumber zero];
        } else {
            // Now re-parse value - we need exact value
            NSLocale *locale = [NSLocale currentLocale];
            decimalAmount = [NSDecimalNumber decimalNumberWithString:trimmedText locale:locale];
        }
        enteredAmount = decimalAmount;
    }
    
    [self processOutcomeAmount:enteredAmount];
    [self notifyOutcomeAmountChanged];
}

#pragma mark - UITextFieldDelegate implementation

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Change pair just after begining of edit
    if (self.textField.text.length == 0) {
        [self processOutcomeAmount:[NSDecimalNumber zero]];
        [self notifyDidStartEditOutcome];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
                                    replacementString:(NSString *)string {
    
    NSString *prefixString = [NSString stringWithCharacters:&kOutcomePrefixSign length:1];
    
    //  Insert prefix when user start editing
    if (textField.text.length == 0 && string.length > 0) {
        BOOL shouldAddPrefix = [string characterAtIndex:0] != kOutcomePrefixSign; //  Don't add prefix if it already exists
        if (shouldAddPrefix) {
            self.textField.text = [prefixString stringByAppendingString:string];
        } else {
            self.textField.text = string;
        }
        [textField sendActionsForControlEvents:UIControlEventEditingChanged];
        return NO;
    }
    
    // Here our text must contain prefix
    
    // If user wants to modify prefix
    if (range.location == 0) {
        
        // User wants to erase entire text
        if (range.length == textField.text.length && string.length == 0) {
            return YES;
        }
        
        // Don't allow user to modify prefix
        if (range.length > 1) {
            range.location = 1;
            range.length = range.length - 1;
            self.textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        }
        [textField sendActionsForControlEvents:UIControlEventEditingChanged];
        return NO;
    }
    
    // If user wants to erase entire text exept prefix then just erase all text
    NSString *resultStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if ([resultStr isEqualToString:prefixString]) {
        self.textField.text = @"";
        [textField sendActionsForControlEvents:UIControlEventEditingChanged];
        return NO;
    }
    
    return YES;
}

@end
