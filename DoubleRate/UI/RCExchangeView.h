//
//  RCExchangeView.h
//  Double Rate
//
//  Created by Dmitry on 12.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCExchangeView, RCCurrencyAccount, RCCurrencyConverter;

@protocol RCExchangeViewDelegate <NSObject>

@optional

// Notifies that currency pair has been updated.
- (void)exchangeViewDidUpdatePair:(RCExchangeView *)exchangeView;

@end

// Represents view that displays two accounts
// User can switch accounts using carousel
@interface RCExchangeView : UIView

@property (nonatomic, weak) id<RCExchangeViewDelegate> delegate;

// Allows set currency converter
@property (nonatomic, strong) RCCurrencyConverter *conveter;

// Returns currently selected source and destinations accounts
// Returns nil if no account selected
// Every time one of the account IS not be nil
@property (nonatomic, readonly) RCCurrencyAccount *sourceAccount;
@property (nonatomic, readonly) RCCurrencyAccount *destinationAccount;

// Returns currently selected amount
// Returns nil if amount is not selected
@property (nonatomic, readonly) NSDecimalNumber *amount;

// Pass YES when keyboard is visible on screen
@property (nonatomic, assign) BOOL shouldKeepKeyboardOnReload;

// Loads most resent curreny rates and makes recalculation
- (void)updateRates;

// Reset any input and update balances
- (void)reset;

@end
