//
//  RCExchangePanel.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCExchangePanel.h"
#import "RCCurrencyConverter.h"
#import "RCCurrencyUtils.h"

static const CGFloat kControlsTopOffset = 10.0;
static const CGFloat kFontSize = 14.;
static const CGFloat kButtonFontSize = 16.;

@interface RCExchangePanel ()
@property (nonatomic, strong) UIButton *exchangeButton;
@property (nonatomic, strong) UIView *loadingPanel;

@property (nonatomic, strong) UIView *infoPanel;
@property (nonatomic, strong) UILabel *rateLabel;

@property (nonatomic, strong) NSString *targetCode;
@property (nonatomic, strong) NSString *sourceCode;
@end

@implementation RCExchangePanel

#pragma mark - Memory Management and Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self buildViewHierarchy];
    }
    return self;
}

#pragma mark - Public Interface

- (void)showRatesLoading:(BOOL)show {
    BOOL prevLoadingPanelHidden = self.loadingPanel.hidden;
    self.loadingPanel.hidden = !show;
    self.infoPanel.hidden = show;
    
    BOOL shouldAnimate = prevLoadingPanelHidden != self.loadingPanel.hidden;
    if (shouldAnimate) {
        [UIView transitionWithView:self duration:[CATransaction animationDuration]
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{} completion:nil];
    }
}

- (void)showRateForCurrency:(NSString *)code1 relativeCurrency:(NSString *)code2 {
    NSAssert(code1.length > 0 && code2.length > 0, @"");
    NSAssert(self.converter != nil, @"");
    
    self.targetCode = code1;
    self.sourceCode = code2;
    
    NSString *rateText = @"";
    
    NSDecimalNumber *rate = [self.converter rateForCurrency:code1 relativeCurrency:code2];
    if (!rate) {
        rateText = NSLocalizedString(@"RATE_UNKNOWN", nil);
    } else {
        NSString *destSign = signForCurrency(code1);
        NSString *sourceSign = signForCurrency(code2);
        NSString *sourceRate = [rate descriptionWithLocale:[NSLocale currentLocale]];
        rateText = [NSString stringWithFormat:@"%@1 = %@%@", destSign, sourceSign, sourceRate];
    }
    
    self.rateLabel.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"RATE_IS", nil), rateText];
    [self.rateLabel sizeToFit];
}

- (void)updateRate {
    if (self.targetCode && self.sourceCode) {
        [self showRateForCurrency:self.targetCode relativeCurrency:self.sourceCode];
    }
}

#pragma mark - Inner Logic

#pragma mark - View Hierarchy

- (void)buildViewHierarchy {
    const CGFloat kBackColor = 0.97;
    self.backgroundColor = [UIColor colorWithWhite:kBackColor alpha:1];
    
    [self buildLoadingPanel];
    self.loadingPanel.hidden = YES;
    
    [self buildInfoPanel];
    [self buildBottomSeparator];
}

- (void)buildLoadingPanel {
    
    const CGFloat kLabelPadding = 6;
    const CGFloat kSpinnerPadding = 4;
    
    CGSize panelSize = self.bounds.size;
    
    UIView *loadingPanel = [[UIView alloc] initWithFrame:self.bounds];
    loadingPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    loadingPanel.backgroundColor = [UIColor clearColor];
    [self addSubview:loadingPanel];
    
    //  Add activity indicator
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner startAnimating];
    [loadingPanel addSubview:spinner];
    
    //  Add loading label
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = NSLocalizedString(@"LOADING_RATES", nil);
    label.textColor = [UIColor blackColor];
    label.numberOfLines = 1;
    label.font  = [UIFont systemFontOfSize:kFontSize];
    label.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [label sizeToFit];
    [loadingPanel addSubview:label];
    
    // Layout indicator and label
    
    CGRect labelFrame = label.frame;
    labelFrame.size.width = ceilf(labelFrame.size.width);
    labelFrame.size.height = ceilf(label.bounds.size.height);
    labelFrame.origin.x = kLabelPadding;
    labelFrame.origin.y = ceilf((panelSize.height - labelFrame.size.height) / 2.) + kControlsTopOffset;
    
    label.frame = labelFrame;
    
    CGRect spinnerFrame = spinner.frame;
    spinnerFrame.origin.x = CGRectGetMaxX(label.frame) + kSpinnerPadding;
    spinnerFrame.origin.y = ceilf((panelSize.height - spinnerFrame.size.height) / 2.) + kControlsTopOffset;
    spinner.frame = spinnerFrame;
    
    self.loadingPanel = loadingPanel;
}

- (void)buildInfoPanel {
    const CGFloat kLabelPadding = 6;
    const CGFloat kButtonPadding = 6;
    
    CGSize panelSize = self.bounds.size;
    
    UIView *infoPanel = [[UIView alloc] initWithFrame:self.bounds];
    infoPanel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    infoPanel.backgroundColor = [UIColor clearColor];
    [self addSubview:infoPanel];
    
    // Add info label
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"RATE_IS", nil), NSLocalizedString(@"RATE_UNKNOWN" , nil)];
    label.textColor = [UIColor blackColor];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:kFontSize];
    label.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [label sizeToFit];
    [infoPanel addSubview:label];
    self.rateLabel = label;
    
    // Layout label
    
    CGRect labelFrame = label.frame;
    labelFrame.size.width = ceilf(labelFrame.size.width);
    labelFrame.size.height = ceilf(labelFrame.size.height);
    labelFrame.origin.x = kLabelPadding;
    labelFrame.origin.y = ceilf((panelSize.height - labelFrame.size.height) / 2.) + kControlsTopOffset;
    
    label.frame = labelFrame;
    
    // Add exchange button
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [button setTitle:NSLocalizedString(@"DO_EXCHANGE", nil) forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont systemFontOfSize:kButtonFontSize];
    [button addTarget:self action:@selector(onExchangeButtonDidTap) forControlEvents:UIControlEventTouchUpInside];
    button.exclusiveTouch = YES;
    [button sizeToFit];
    [infoPanel addSubview:button];
    
    // Layout button
    
    CGRect buttonFrame = button.frame;
    buttonFrame.size.width = ceilf(buttonFrame.size.width);
    buttonFrame.size.height = ceilf(buttonFrame.size.height);
    buttonFrame.origin.x = panelSize.width - kButtonPadding - buttonFrame.size.width;
    buttonFrame.origin.y = ceilf((panelSize.height - buttonFrame.size.height) / 2.) + kControlsTopOffset;
    
    button.frame = buttonFrame;
    
    self.infoPanel = infoPanel;
}

- (void)buildBottomSeparator {
    const CGFloat kSeparatorHeight = 1.;
    const CGFloat kSeparatorColor = 0.84;
    
    UIView *separator = [[UIView alloc] initWithFrame:
                         CGRectMake(0., self.bounds.size.height - kSeparatorHeight,
                                    self.bounds.size.width, kSeparatorHeight)];
    separator.backgroundColor = [UIColor colorWithWhite:kSeparatorColor alpha:1.];
    separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self addSubview:separator];
}

#pragma mark - User Actions

- (void)onExchangeButtonDidTap {
    if (self.onExchangeDidTap) {
        self.onExchangeDidTap();
    }
}

@end
