//
//  RCExchangeViewController.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCExchangeViewController.h"
#import "RCCurrencyRatesLoader.h"
#import "RCCurrencyAccount.h"
#import "RCCurrencyConsts.h"
#import "RCCurrencyUtils.h"
#import "RCCurrencyConverter.h"
#import "RCAccountManager.h"
#import "RCExchangePanel.h"
#import "RCExchangeView.h"
#import "NSDecimalNumber+RCUtils.h"


static const CGFloat kPanelHeight = 60.;

@interface RCExchangeViewController () <RCExchangeViewDelegate>
@property (nonatomic, strong) RCCurrencyConverter *converter;
@property (nonatomic, strong) RCExchangePanel *ratePanel;
@property (nonatomic, strong) RCExchangeView *exchangeView;

@property (nonatomic, assign) BOOL isKeyboardActive;
@end

@implementation RCExchangeViewController

#pragma mark - Memory Management and Initialization

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];

    [self buildViewHierarchy];
    [self subscribeKeyboardNotifications];
    
    [self setupCurrencyConverter];
    self.ratePanel.converter = self.converter;
    self.exchangeView.conveter = self.converter;
    
    [self setupCurrencyLoader];
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Inner Logic

- (void)setupCurrencyConverter {
    self.converter = [RCCurrencyConverter new];
}

- (void)setupCurrencyLoader {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRatesUpdated:)
                                                 name:RCCurrencyRatesUpdatedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onRatesUpdateFail:)
                                                 name:RCCurrencyRatesUpdateFailNotification object:nil];
    
    BOOL emptyRates = [RCCurrencyRatesLoader sharedInstance].rates.count == 0;
    [[RCCurrencyRatesLoader sharedInstance] start];
    
    if (emptyRates) {
        [self.ratePanel showRatesLoading:YES];
    }
}

- (BOOL)validateExchange {
    
    if ([self.exchangeView.sourceAccount.currencyCode
         isEqualToString:self.exchangeView.destinationAccount.currencyCode]) {
        [self showErrorWithText:NSLocalizedString(@"ACCOUNTS_MUST_BE_DIFFERENT", nil)];
        return NO;
    }
    
    if (self.exchangeView.amount == nil || [self.exchangeView.amount rc_isZero]) {
        [self showErrorWithText:NSLocalizedString(@"ENTER_THE_AMOUNT_TO_CONVERT", nil)];
        return NO;
    }
    
    NSAssert(self.exchangeView.sourceAccount != nil && self.exchangeView.destinationAccount != nil, @"");
    
    NSDecimalNumber *minAmount = [[RCAccountManager sharedInstance] minAmountForExchangeForCurrency:
                                  self.exchangeView.sourceAccount.currencyCode];
    
    if ([self.exchangeView.amount rc_lessThan:minAmount]) {
        NSString *sign = signForCurrency(self.exchangeView.sourceAccount.currencyCode);
        NSString *errText = [NSString stringWithFormat:@"%@ %@%@",
                             NSLocalizedString(@"MIN_AMOUNT_TO_CONVERT_IS", nil),
                             sign, [minAmount descriptionWithLocale:[NSLocale currentLocale]]];
        [self showErrorWithText:errText];
        return NO;
    }
    
    return YES;
}

- (void)exchangeSelectedAccounts {
    RCCurrencyAccount *sourceAccount = self.exchangeView.sourceAccount;
    RCCurrencyAccount *destinationAccount = self.exchangeView.destinationAccount;
    NSDecimalNumber *amount = self.exchangeView.amount;
    
    RCExchangeViewController * __weak weakSelf = self;
    [[RCAccountManager sharedInstance] exchangeFromSource:sourceAccount
                                              destination:destinationAccount
                                                   amount:amount
                                                converter:self.converter
    completion:^(BOOL success, NSError *error) {
        if (success) {
            [weakSelf processExchangeSuccess];
        } else {
            [weakSelf processExchangeError:error];
        }
    }];
}

- (void)processExchangeSuccess {
    [self showInfoAlertWithText:@"Operation is complete successfully."];
    [self.exchangeView reset];
}

- (void)processExchangeError:(NSError *)error {
    NSString *errorText = NSLocalizedString(@"EXCHANGE_FAILED", nil);
    if ([error.domain isEqualToString:kRCAccountManagerErrorDomain] && error.code == RCInsufficientFundsError) {
        errorText = NSLocalizedString(@"INSUFFICIENT_FUNDS", nil);
    }
    [self showErrorWithText:errorText];
}

#pragma mark - Notifications

- (void)subscribeKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onKeyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)onRatesUpdated:(NSNotification *)notif {
    [self.converter update];
    [self.ratePanel showRatesLoading:NO];
    [self.ratePanel updateRate];
    [self.exchangeView updateRates];
}

- (void)onRatesUpdateFail:(NSNotification *)notif {
    // Do nothing at the current moment
}

- (void)onKeyboardWillShow:(NSNotification *)notif {
    self.isKeyboardActive = YES;
    CGRect endFrame = [notif.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval duration = [notif.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect viewRect = [self viewRect:self.view fromScreenRect:endFrame];
    
    CGFloat newHeight = viewRect.origin.y - kPanelHeight;
    [self changeExchangeViewHeight:newHeight animated:YES duration:duration];
}

- (void)onKeyboardWillHide:(NSNotification *)notif {
    self.isKeyboardActive = NO;
    NSTimeInterval duration = [notif.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGFloat newHeight = self.view.bounds.size.height - kPanelHeight;
    [self changeExchangeViewHeight:newHeight animated:YES duration:duration];
}

#pragma mark - View Hierarchy

- (void)buildViewHierarchy {
    [self.navigationController setNavigationBarHidden:YES];
    [self buildRatePanel];
    [self buildExchangeView];
}

- (void)buildRatePanel {
    RCExchangePanel *panel = [[RCExchangePanel alloc] initWithFrame: CGRectMake(0., 0., self.view.bounds.size.width, kPanelHeight)];
    panel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:panel];
    
    RCExchangeViewController * __weak weakSelf = self;
    panel.onExchangeDidTap = ^{
        [weakSelf onExchangeDidTap];
    };
    
    self.ratePanel = panel;
}

- (void)buildExchangeView {
    CGSize selfBounds = self.view.bounds.size;
    RCExchangeView *view = [[RCExchangeView alloc] initWithFrame:CGRectMake(0., kPanelHeight,
                            selfBounds.width, selfBounds.height - kPanelHeight)];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:view];
    
    view.delegate = self;
    self.exchangeView = view;
}

- (void)changeExchangeViewHeight:(CGFloat)height animated:(BOOL)animated
                        duration:(NSTimeInterval)animationDuration {
    
    dispatch_block_t updateBlock = ^{
        CGRect frame = self.exchangeView.frame;
        frame.size.height = height;
        
        BOOL needLayoutSubviews = !CGRectEqualToRect(frame, self.exchangeView.frame);
        self.exchangeView.frame = frame;
        self.exchangeView.shouldKeepKeyboardOnReload = self.isKeyboardActive;
        if (needLayoutSubviews) {
            [self.exchangeView layoutSubviews];  // We need it for nice carousels size animation
        }
    };
    
    if (animated) {
        [UIView animateWithDuration:animationDuration delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:updateBlock completion:nil];
    } else {
        updateBlock();
    }
}

- (CGRect)viewRect:(UIView *)view fromScreenRect:(CGRect)screenRect {
    CGRect windowRect = [view.window convertRect:screenRect fromWindow:nil];
    return [view convertRect:windowRect fromView:nil];
}

#pragma mark - User Actions

- (void)onExchangeDidTap {
    if ([self validateExchange]) {
        [self exchangeSelectedAccounts];
    }
}

- (void)showErrorWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:NSLocalizedString(@"ERROR" , nil)
                                message:text
                                preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", nil)
                                                           style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showInfoAlertWithText:(NSString *)text {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:text
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelButton];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - RCExchangeViewDelegate implemention

- (void)exchangeViewDidUpdatePair:(RCExchangeView *)exchangeView {
    NSString *sourceCurrency = exchangeView.sourceAccount.currencyCode;
    NSString *destinationCurrency = exchangeView.destinationAccount.currencyCode;
    [self.ratePanel showRateForCurrency:destinationCurrency relativeCurrency:sourceCurrency];
}

@end
