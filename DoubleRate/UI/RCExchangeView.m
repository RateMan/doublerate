//
//  RCExchangeView.m
//  Double Rate
//
//  Created by Dmitry on 12.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "RCExchangeView.h"
#import "iCarousel.h"
#import "RCCurrencyConverter.h"
#import "RCAccountManager.h"
#import "RCCurrencyAccount.h"
#import "RCAccountView.h"
#import "NSDecimalNumber+RCUtils.h"

static const CGFloat kBackColor = 0.94;
static const CGSize kArrowSize = {100., 16.};

static const CGFloat kPageControlBottom = 8.;

@interface RCExchangeView () <iCarouselDataSource, iCarouselDelegate, RCAccountViewDelegate>
@property (nonatomic, strong) NSArray<RCCurrencyAccount *> *topAccountList;
@property (nonatomic, strong) NSArray<RCCurrencyAccount *> *bottomAccountList;

@property (nonatomic, strong) NSDecimalNumber *topOutcomeAmount;
@property (nonatomic, strong) NSDecimalNumber *bottomOutcomeAmount;

@property (nonatomic, strong) iCarousel *topCarousel;
@property (nonatomic, strong) iCarousel *bottomCarousel;

@property (nonatomic, strong) UIView *topArrowView;
@property (nonatomic, strong) UIView *bottomArrowView;

@property (nonatomic, strong) UITextField *keyboardKeeper;
@property (nonatomic, strong) iCarousel  *lastResponderCarousel;

@property (nonatomic, strong) UIPageControl *topPageControl;
@property (nonatomic, strong) UIPageControl *bottomPageControl;

@end

@implementation RCExchangeView

#pragma mark - Memory Management and Initialization

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        [self loadAccountList];
        [self buildViewHierarchy];
        
        if (self.topAccountList.count > 0) {
            [self reloadCarousels];
            
            // We need dispath because here delegate is not set
            dispatch_async(dispatch_get_main_queue(), ^{
                // Default source is top cell
                self.topOutcomeAmount = [NSDecimalNumber zero];
                [self processPairDidChange];
            });
            
        } else {
            self.topCarousel.dataSource = nil;
            self.bottomCarousel.dataSource = nil;
            
            self.topCarousel.delegate = nil;
            self.bottomCarousel.delegate = nil;
        }
    }
    return self;
}

- (void)buildViewHierarchy {
    self.backgroundColor = [UIColor colorWithWhite:kBackColor alpha:1];
    [self buildKeyboarKeeper];
    [self buildCarousels];
    [self buildArrows];
    [self buildPageControls];
}

- (void)buildCarousels {
    self.topCarousel = [self buildCarousel];
    self.bottomCarousel = [self buildCarousel];
    
    self.bottomCarousel.backgroundColor = [UIColor colorWithWhite:0.89 alpha:1];
}

- (void)buildArrows {
    
    //  Build bottom arrow
    UIBezierPath * bottomArrowPath = [UIBezierPath bezierPath];
    [bottomArrowPath moveToPoint:CGPointZero];
    [bottomArrowPath addLineToPoint:CGPointMake(kArrowSize.width / 2., kArrowSize.height)];
    [bottomArrowPath addLineToPoint:CGPointMake(kArrowSize.width, 0.)];
    [bottomArrowPath closePath];

    CAShapeLayer *bottomArrowLayer = [CAShapeLayer layer];
    bottomArrowLayer.path = bottomArrowPath.CGPath;
    bottomArrowLayer.fillColor = self.topCarousel.backgroundColor.CGColor;
    
    self.bottomArrowView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kArrowSize.width, kArrowSize.height)];
    [self.bottomArrowView.layer addSublayer:bottomArrowLayer];
    
    //  Build top arrow
    UIBezierPath * topArrowPath = [UIBezierPath bezierPath];
    [topArrowPath moveToPoint:CGPointMake(0., kArrowSize.height)];
    [topArrowPath addLineToPoint:CGPointMake(kArrowSize.width / 2., 0.)];
    [topArrowPath addLineToPoint:CGPointMake(kArrowSize.width, kArrowSize.height)];
    [topArrowPath closePath];
    
    CAShapeLayer *topArrowLayer = [CAShapeLayer layer];
    topArrowLayer.path = topArrowPath.CGPath;
    topArrowLayer.fillColor = self.bottomCarousel.backgroundColor.CGColor;
    
    self.topArrowView = [[UIView alloc] initWithFrame:CGRectMake(0., 0., kArrowSize.width, kArrowSize.height)];
    [self.topArrowView.layer addSublayer:topArrowLayer];
    
    
    [self addSubview:self.bottomArrowView];
    [self addSubview:self.topArrowView];
    
    self.topArrowView.hidden = YES;
    self.bottomArrowView.hidden = YES;
}

- (void)buildKeyboarKeeper {
    self.keyboardKeeper = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    self.keyboardKeeper.keyboardType = UIKeyboardTypeDecimalPad;
    self.keyboardKeeper.autocorrectionType = UITextAutocorrectionTypeNo;
    [self addSubview:self.keyboardKeeper];
}

- (iCarousel *)buildCarousel {
    iCarousel *carousel = [[iCarousel alloc] initWithFrame:CGRectZero];
    carousel.backgroundColor = [UIColor colorWithWhite:kBackColor alpha:1];
    carousel.exclusiveTouch = YES;
    carousel.pagingEnabled = YES;
    carousel.type = iCarouselTypeLinear;
    carousel.dataSource = self;
    carousel.delegate = self;
    [self addSubview:carousel];
    return carousel;
}

- (void)buildPageControls {
    self.topPageControl = [self buildPageControl];
    self.bottomPageControl = [self buildPageControl];
    
    self.topPageControl.numberOfPages = self.topAccountList.count;
    self.bottomPageControl.numberOfPages = self.bottomAccountList.count;
    
    [self.topPageControl sizeToFit];
    [self.bottomPageControl sizeToFit];
}

- (UIPageControl *)buildPageControl {
    UIPageControl *page = [UIPageControl new];
    page.pageIndicatorTintColor = [UIColor colorWithWhite:0.6 alpha:0.5];
    page.currentPageIndicatorTintColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    page.userInteractionEnabled = NO;
    [self addSubview:page];
    return page;
}


#pragma mark - Public Interface

- (RCCurrencyAccount *)sourceAccount {
    NSAssert(self.topOutcomeAmount == nil || self.bottomOutcomeAmount == nil, @"");
    
    if (self.topOutcomeAmount != nil) {
        return [self currentAccountForCarousel:self.topCarousel];
        
    } else if (self.bottomOutcomeAmount != nil) {
        return [self currentAccountForCarousel:self.bottomCarousel];
        
    } else {
        return [self currentAccountForCarousel:self.topCarousel];
    }
}

- (RCCurrencyAccount *)destinationAccount {
    NSAssert(self.topOutcomeAmount == nil || self.bottomOutcomeAmount == nil, @"");
    
    if (self.topOutcomeAmount != nil) {
        return [self currentAccountForCarousel:self.bottomCarousel];
        
    } else if (self.bottomOutcomeAmount != nil) {
        return [self currentAccountForCarousel:self.topCarousel];
        
    } else {
        return [self currentAccountForCarousel:self.bottomCarousel];
    }
}

- (NSDecimalNumber *)amount {
    return self.topOutcomeAmount ?: self.bottomOutcomeAmount;
}

- (void)updateRates {
    if ([self.amount rc_isZero]) {
        return;
    }
    
    iCarousel *carousel = self.topOutcomeAmount ? self.bottomCarousel : self.topCarousel;
    for (NSInteger i = 0; i < carousel.numberOfItems; i++) {
        RCAccountView *accountView = (RCAccountView *)[carousel itemViewAtIndex:i];
        [self updateIncomeAmountInCell:accountView withSourceAccount:self.sourceAccount];
    }
}

- (void)reset {
    [self endEditing:YES];
    self.lastResponderCarousel = nil;
    
    self.topOutcomeAmount = [NSDecimalNumber zero];
    self.bottomOutcomeAmount = nil;
    
    [self updateBalances];
    [self updateArrows];
}

- (void)updateBalances {
    for (RCCurrencyAccount *account in self.topAccountList) {
        [account refresh];
    }
    
    for (RCCurrencyAccount *account in self.bottomAccountList) {
        [account refresh];
    }
    
    [self.topCarousel reloadData];
    [self.bottomCarousel reloadData];
}


#pragma mark - Inner Logic

- (void)loadAccountList {
    NSArray<RCCurrencyAccount *> *accountList = [[RCAccountManager sharedInstance] loadAvailableAccounts];
    self.topAccountList = accountList;
    self.bottomAccountList = [[accountList reverseObjectEnumerator] allObjects];
}

- (void)reloadCarousels {
    [self.topCarousel reloadData];
    [self.bottomCarousel reloadData];
}

- (RCAccountView *)currentViewInCarousel:(iCarousel *)carousel {
    NSInteger curentIndex = carousel.currentItemIndex;
    if ([carousel numberOfItems] > 0 && curentIndex != -1) {
        return (RCAccountView *)[carousel itemViewAtIndex:curentIndex];
    }
    return nil;
}

- (RCCurrencyAccount *)currentAccountForCarousel:(iCarousel *)carousel {
    NSArray<RCCurrencyAccount *> *accountList = (carousel == self.topCarousel) ?
                                                self.topAccountList : self.bottomAccountList;
    
    NSInteger currentIndex = carousel.currentItemIndex;
    if (accountList.count > 0 && currentIndex != -1) {
        return accountList[currentIndex];
    }
    return nil;
}

- (void)processPairDidChange {
    [self updateArrows];
    [self notifyPairDidChange];
}

- (void)processOutcomeAmountDidChangeInView:(RCAccountView *)accountView {
    NSAssert(accountView.outcomeAmount != nil, @"");
    
    BOOL pairChanged = NO;
    RCAccountView *destinationView;
    
    if ([self isCell:accountView currentForCarousel:self.topCarousel]) {
        pairChanged = (self.topOutcomeAmount == nil);
        self.topOutcomeAmount = accountView.outcomeAmount;
        self.bottomOutcomeAmount = nil;
        destinationView = [self currentViewInCarousel:self.bottomCarousel];
        
    } else if ([self isCell:accountView currentForCarousel:self.bottomCarousel]) {
        pairChanged = (self.bottomOutcomeAmount == nil);
        self.bottomOutcomeAmount = accountView.outcomeAmount;
        self.topOutcomeAmount = nil;
        destinationView = [self currentViewInCarousel:self.topCarousel];
        
    } else {
        pairChanged = YES;
        self.topOutcomeAmount = nil;
        self.bottomOutcomeAmount = nil;
        NSAssert(NO, @"Should not reach");
    }
    
    if (pairChanged) {
        [self processPairDidChange];
    }
    
    // Update destination view
    [self updateIncomeAmountInCell:destinationView withSourceAccount:self.sourceAccount];
    
    NSAssert(self.topOutcomeAmount == nil || self.bottomOutcomeAmount == nil, @"");
}

- (void)notifyPairDidChange {
    if ([self.delegate respondsToSelector:@selector(exchangeViewDidUpdatePair:)]) {
        [self.delegate exchangeViewDidUpdatePair:self];
    }
}

- (void)updateArrows {
    self.topArrowView.hidden = (self.topOutcomeAmount != nil);
    self.bottomArrowView.hidden = (self.bottomOutcomeAmount != nil);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //  Layout carousels
    CGSize selfSize = self.bounds.size;
    
    CGRect topFrame;
    topFrame.origin = CGPointMake(0., 0.);
    topFrame.size = CGSizeMake(selfSize.width, ceilf(selfSize.height / 2.));
    self.topCarousel.frame = topFrame;
    
    CGRect bottomFrame;
    bottomFrame.origin = CGPointMake(0., CGRectGetMaxY(topFrame));
    bottomFrame.size = CGSizeMake(selfSize.width, topFrame.size.height);
    self.bottomCarousel.frame = bottomFrame;
    
    [self reloadCarouselsAndKeepKeyboard];
    
    // Layout arrows
    CGRect bottomArrowFrame = CGRectZero;
    bottomArrowFrame.size = kArrowSize;
    bottomArrowFrame.origin.y = CGRectGetMaxY(self.topCarousel.frame);
    bottomArrowFrame.origin.x = ceilf((selfSize.width - kArrowSize.width) / 2.);
    self.bottomArrowView.frame = bottomArrowFrame;
    
    CGRect topArrowFrame = CGRectZero;
    topArrowFrame.size = kArrowSize;
    topArrowFrame.origin.y = CGRectGetMaxY(self.topCarousel.frame) - kArrowSize.height;
    topArrowFrame.origin.x = ceilf((selfSize.width - kArrowSize.width) / 2.);
    self.topArrowView.frame = topArrowFrame;
    
    //  Layout page controls
    CGRect pageFrame = self.topPageControl.bounds;
    pageFrame.origin.y = ceilf(selfSize.height / 2.) - pageFrame.size.height - kPageControlBottom;
    pageFrame.origin.x = ceilf((selfSize.width - pageFrame.size.width) / 2.);
    self.topPageControl.frame = pageFrame;
    
    pageFrame = self.bottomPageControl.bounds;
    pageFrame.origin.y = selfSize.height - pageFrame.size.height - kPageControlBottom;
    pageFrame.origin.x = ceilf((selfSize.width - pageFrame.size.width) / 2.);
    self.bottomPageControl.frame = pageFrame;
}

- (void)reloadCarouselsAndKeepKeyboard {
    
    BOOL firstResponderIsTop = [self firstResponderStatusForCarousel:self.topCarousel];
    BOOL firstResponderIsBottom = [self firstResponderStatusForCarousel:self.bottomCarousel];
    if (self.shouldKeepKeyboardOnReload && (firstResponderIsTop || firstResponderIsBottom)) {
        [self.keyboardKeeper becomeFirstResponder];
    }
    
    // Request carousels to change items size
    [self.topCarousel reloadData];
    [self.bottomCarousel reloadData];
    
    if (self.shouldKeepKeyboardOnReload) {
        //  Ask carousel to create views
        [self.topCarousel layoutIfNeeded];
        [self.bottomCarousel layoutIfNeeded];
        
        if (firstResponderIsTop) {
            [self becomeCarouselFirstResponder:self.topCarousel];
        } else if (firstResponderIsBottom) {
            [self becomeCarouselFirstResponder:self.bottomCarousel];
        }
    }
}

- (BOOL)firstResponderStatusForCarousel:(iCarousel *)carousel {
    NSInteger curentIndex = carousel.currentItemIndex;
    if ([carousel numberOfItems] > 0 && curentIndex != -1) {
        return [carousel itemViewAtIndex:curentIndex].isFirstResponder;
    }
    return NO;
}

- (void)becomeCarouselFirstResponder:(iCarousel *)carousel {
    NSInteger curentIndex = carousel.currentItemIndex;
    if ([carousel numberOfItems] > 0 && curentIndex != -1) {
        [[carousel itemViewAtIndex:curentIndex] becomeFirstResponder];
    }
}

- (void)restoreLastFirstResponderCarousel:(iCarousel *)carousel {
    if (carousel != self.lastResponderCarousel) {
        return;
    }
    
    if (self.lastResponderCarousel) {
        [self becomeCarouselFirstResponder:self.lastResponderCarousel];
        self.lastResponderCarousel = nil;
    }
    [self.keyboardKeeper resignFirstResponder];
}

- (BOOL)isCell:(UIView *)cell currentForCarousel:(iCarousel *)carousel {
    NSInteger curentIndex = carousel.currentItemIndex;
    if ([carousel numberOfItems] > 0 && curentIndex != -1) {
        return ([carousel itemViewAtIndex:curentIndex] == cell);
    }
    return NO;
}

- (void)updateIncomeAmountInCell:(RCAccountView *)cell
               withSourceAccount:(RCCurrencyAccount *)sourceAccount {
    
    NSAssert(self.topOutcomeAmount == nil || self.bottomOutcomeAmount == nil, @"");
    NSAssert(self.amount != nil, @"");
    NSDecimalNumber *convertedAmount = [self.conveter convertAmount:self.amount
                                                 fromSourceCurrency:sourceAccount.currencyCode
                                              toDestinationCurrency:cell.account.currencyCode];
    //  Rates can be still not loaded here
    if (convertedAmount != nil) {
        cell.incomeAmount = convertedAmount;
    } else {
        cell.incomeAmount = [NSDecimalNumber zero];
    }
}

#pragma mark - RCAccountViewDelegate

- (void)accountViewDidChangeOutcomeAmount:(RCAccountView *)accountView {
    [self processOutcomeAmountDidChangeInView:accountView];
}

- (void)accountViewDidStartEditOutcomeAmount:(RCAccountView *)accountView {
    [self processOutcomeAmountDidChangeInView:accountView];
}


#pragma mark - iCarouselDataSource implementation

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    if (carousel == self.topCarousel) {
        return self.topAccountList.count;
    } else {
        return self.bottomAccountList.count;
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index
         reusingView:(nullable UIView *)view {
    const CGFloat kInitialViewSize = 10.;
    
    NSArray<RCCurrencyAccount *> *accountList = (carousel == self.topCarousel) ? self.topAccountList : self.bottomAccountList;
    RCCurrencyAccount *account = accountList[index];
    
    RCAccountView *accountView = (RCAccountView *)view;
    if (accountView == nil) {
        accountView = [[RCAccountView alloc] initWithFrame:
                       CGRectMake(0, 0, kInitialViewSize, kInitialViewSize)];
        accountView.delegate = self;
    } else {
        NSAssert([view isKindOfClass:[RCAccountView class]], @"");
    }
    
    CGRect cellRect = CGRectZero;
    cellRect.size = carousel.bounds.size;
    accountView.frame = cellRect;
    
    accountView.account = account;
    
    if (carousel == self.topCarousel) {
        
        if (self.topOutcomeAmount != nil) {
            accountView.outcomeAmount = self.topOutcomeAmount;
            
        } else if (self.bottomOutcomeAmount != nil && ![self.bottomOutcomeAmount rc_isZero] &&
                   self.bottomCarousel.currentItemIndex != -1) {
            
            [self updateIncomeAmountInCell:accountView
                         withSourceAccount:[self currentAccountForCarousel:self.bottomCarousel]];
        }
    }
    
    if (carousel == self.bottomCarousel) {
        if (self.bottomOutcomeAmount != nil) {
            accountView.outcomeAmount = self.bottomOutcomeAmount;
            
        } else if (self.topOutcomeAmount != nil && ![self.topOutcomeAmount rc_isZero] &&
                   self.topCarousel.currentItemIndex != -1) {
            
            [self updateIncomeAmountInCell:accountView
                         withSourceAccount:[self currentAccountForCarousel:self.topCarousel]];
        }
    }
    
    return accountView;
}

#pragma mark - iCarouselDelegate implementation

- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel {
    NSAssert(self.topOutcomeAmount == nil || self.bottomOutcomeAmount == nil, @"");
    
    [self processPairDidChange];
    
    if (carousel == self.topCarousel) {
        if (self.topOutcomeAmount != nil) {
            RCAccountView *cell = (RCAccountView *)carousel.currentItemView;
            cell.outcomeAmount = self.topOutcomeAmount;
            [self updateIncomeAmountInCell:(RCAccountView *)self.bottomCarousel.currentItemView
                         withSourceAccount:self.sourceAccount];
        } else if (self.bottomOutcomeAmount != nil) {
            [self updateIncomeAmountInCell:(RCAccountView *)carousel.currentItemView
                         withSourceAccount:self.sourceAccount];
        }
        
        self.topPageControl.currentPage = carousel.currentItemIndex;
    }
    
    if (carousel == self.bottomCarousel) {
        if (self.bottomOutcomeAmount != nil) {
            RCAccountView *cell = (RCAccountView *)carousel.currentItemView;
            cell.outcomeAmount = self.bottomOutcomeAmount;
            [self updateIncomeAmountInCell:(RCAccountView *)self.topCarousel.currentItemView
                         withSourceAccount:self.sourceAccount];
        } else if (self.topOutcomeAmount != nil) {
            [self updateIncomeAmountInCell:(RCAccountView *)carousel.currentItemView
             withSourceAccount:self.sourceAccount];
        }
        
        self.bottomPageControl.currentPage = carousel.currentItemIndex;
    }
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    if (option == iCarouselOptionWrap) {
        return 1.;
    }
    return value;
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel {
    [self restoreLastFirstResponderCarousel:carousel];
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel {
    if ([self firstResponderStatusForCarousel:carousel]) {
        self.lastResponderCarousel = carousel;
        [self.keyboardKeeper becomeFirstResponder];
    }
}

@end
