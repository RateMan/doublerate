//
//  RCAccountView.h
//  Double Rate
//
//  Created by Dmitry on 12.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCAccountView, RCCurrencyAccount;

@protocol RCAccountViewDelegate <NSObject>
@optional

// User did change outcome amount in cell
- (void)accountViewDidChangeOutcomeAmount:(RCAccountView *)accountView;

// User did start edit empty outcome text field
- (void)accountViewDidStartEditOutcomeAmount:(RCAccountView *)accountView;

@end

// Repsents view that displays account info
// and allows input amount of money to convert.
@interface RCAccountView : UIView

@property (nonatomic, weak) id<RCAccountViewDelegate> delegate;

// Allows set income amout to display
// Mutual excluse with outcomeAmount
@property (nonatomic, strong) NSDecimalNumber *incomeAmount;

// Allows set outcome amount to display in text field
// Mutual excluse with incomeAmount
@property (nonatomic, strong) NSDecimalNumber *outcomeAmount;

//  Account to display
@property (nonatomic, strong) RCCurrencyAccount *account;

@end
