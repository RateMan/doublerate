//
//  RCExchangePanel.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCCurrencyConverter;

// Displays currenct rate and Exchange button.
@interface RCExchangePanel : UIView

//  Returns current displayed currency codes.
@property (nonatomic, readonly) NSString *targetCode;
@property (nonatomic, readonly) NSString *sourceCode;

// Allows set converter that will be used for rates calculations.
@property (nonatomic, strong) RCCurrencyConverter *converter;

// Allows set Exchange button callback.
@property (nonatomic, copy) dispatch_block_t onExchangeDidTap;

// Display how many currency with code 2 we need to buy one currency unit with code 1.
- (void)showRateForCurrency:(NSString *)code1 relativeCurrency:(NSString *)code2;

// Updates rate according last loaded rates.
- (void)updateRate;

// Shows/hide loading indicator.
- (void)showRatesLoading:(BOOL)show;

@end
