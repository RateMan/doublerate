//
//  RCDecimalNumberBehaviors.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

// Used to round balance. It allows 2 digits after decimal.
@interface RCBalanceRoundBehaviour : NSObject<NSDecimalNumberBehaviors>

@end

// Used to round currency rate. It allows 4 digits after decimal.
@interface RCRateRoundBehaviour : NSObject<NSDecimalNumberBehaviors>

@end
