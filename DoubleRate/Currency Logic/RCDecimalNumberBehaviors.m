//
//  RCDecimalNumberBehaviors.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCDecimalNumberBehaviors.h"

@implementation RCBalanceRoundBehaviour

- (NSRoundingMode)roundingMode {
    return NSRoundBankers;
}

- (short)scale {
    return 2;
}

- (nullable NSDecimalNumber *)exceptionDuringOperation:(SEL)operation
                            error:(NSCalculationError)error
                                           leftOperand:(NSDecimalNumber *)leftOperand
                                          rightOperand:(nullable NSDecimalNumber *)rightOperand {
    NSAssert(NO, @"What a problem with round operation?");
    return nil;
}
@end


@implementation RCRateRoundBehaviour

- (NSRoundingMode)roundingMode {
    return NSRoundBankers;
}

- (short)scale {
    return 4;
}

- (nullable NSDecimalNumber *)exceptionDuringOperation:(SEL)operation
                                                 error:(NSCalculationError)error
                                           leftOperand:(NSDecimalNumber *)leftOperand
                                          rightOperand:(nullable NSDecimalNumber *)rightOperand {
    NSAssert(NO, @"What a problem with round operation?");
    return nil;
}

@end
