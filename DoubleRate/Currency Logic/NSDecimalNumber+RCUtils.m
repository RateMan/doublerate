//
//  NSDecimalNumber+RCUtils.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "NSDecimalNumber+RCUtils.h"

@implementation NSDecimalNumber (RCUtils)

- (BOOL)rc_isNotANumber {
    return [self compare:[NSDecimalNumber notANumber]] == NSOrderedSame;
}

- (BOOL)rc_isZero {
    return [self compare:[NSDecimalNumber zero]] == NSOrderedSame;
}

- (BOOL)rc_greaterOrEqualThan:(NSDecimalNumber *)number {
    NSAssert(number != nil, @"");
    NSComparisonResult result = [self compare:number];
    return result == NSOrderedSame || result == NSOrderedDescending;
}

- (BOOL)rc_lessOrEqualThan:(NSDecimalNumber *)number {
    NSAssert(number != nil, @"");
    NSComparisonResult result = [self compare:number];
    return result == NSOrderedSame || result == NSOrderedAscending;
}

- (BOOL)rc_greaterThan:(NSDecimalNumber *)number {
    NSAssert(number != nil, @"");
    NSComparisonResult result = [self compare:number];
    return result == NSOrderedDescending;
}

- (BOOL)rc_lessThan:(NSDecimalNumber *)number {
    NSAssert(number != nil, @"");
    NSComparisonResult result = [self compare:number];
    return result == NSOrderedAscending;
}

@end
