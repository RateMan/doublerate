//
//  RCCurrencyConverter.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCCurrencyConverter.h"
#import "RCCurrencyRatesLoader.h"
#import "RCCurrencyConsts.h"
#import "NSDecimalNumber+RCUtils.h"
#import "RCDecimalNumberBehaviors.h"

@interface RCCurrencyConverter ()
@property (nonatomic, strong) NSDictionary<NSString *, NSDecimalNumber *> *rates;

@end

@implementation RCCurrencyConverter

#pragma mark - Memory Management and Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        [self update];
    }
    return self;
}

#pragma mark - Public Interface

// Returns how many currency with code 2 we need to buy one currency unit with code 1.
- (NSDecimalNumber *)rateForCurrency:(NSString *)code1 relativeCurrency:(NSString *)code2 {
    NSAssert(code1.length > 0 && code2.length > 0, @"");
    
    NSDecimalNumber *rate1Number = self.rates[code1];
    NSDecimalNumber *rate2Number = self.rates[code2];
    
    if (rate1Number == nil || rate2Number == nil) {
        return nil;
    }
    
    NSDecimalNumber *result = [rate2Number decimalNumberByDividingBy:rate1Number];
    if ([result rc_isNotANumber]) {
        return nil;
    }
    
    // Round rate
    RCRateRoundBehaviour *roundBehaviour = [RCRateRoundBehaviour new];
    return [result decimalNumberByRoundingAccordingToBehavior:roundBehaviour];
}

- (void)update {
    self.rates = [RCCurrencyRatesLoader sharedInstance].rates;
}

- (NSDecimalNumber *)convertAmount:(NSDecimalNumber *)amount
                fromSourceCurrency:(NSString *)sourceCode toDestinationCurrency:(NSString *)destinationCode {
   NSAssert(sourceCode.length > 0 && destinationCode.length > 0, @"");
   NSAssert(amount != nil, @"");
    
    NSDecimalNumber *rate = [self rateForCurrency:sourceCode relativeCurrency:destinationCode];
    if (rate == nil) {
        return nil;
    }
    
    return [amount decimalNumberByMultiplyingBy:rate];
}

@end
