//
//  RCCurrencyUtils.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

// Returns sign for currency with code.
NSString *signForCurrency(NSString * code);

