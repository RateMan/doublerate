//
//  RCCurrencyRatesParser.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

//  Accumulate currency rates parsing results.
@interface RCCurrencyRatesParser : NSObject <NSXMLParserDelegate>

// Contains parsed rates. In case of error contains empty dictionary.
@property (nonatomic, readonly) NSDictionary<NSString *, NSDecimalNumber *> *rates;

@end
