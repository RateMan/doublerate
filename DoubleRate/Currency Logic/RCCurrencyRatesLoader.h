//
//  RCCurrencyRatesLoader.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

// Rates updated successfully.
extern NSString * const RCCurrencyRatesUpdatedNotification;

// Rates update failed.
extern NSString * const RCCurrencyRatesUpdateFailNotification;

// Loads currency rates relative EUR from ECB server.
// Makes update every 30 sec.
// Saves last loaded results to local storage.
// Notifies about update via notifications.
@interface RCCurrencyRatesLoader : NSObject

// Returnds shared instance.
+ (RCCurrencyRatesLoader *)sharedInstance;

// Is service running.
@property (nonatomic, readonly) BOOL isRunning;

// Returns last loaded rates relative EUR.
// Key: currency code (see RCCurencyConsts.h).
// Value: rate.
@property (nonatomic, readonly) NSDictionary<NSString *, NSDecimalNumber *> *rates;

//  Returns date of last update.
@property (nonatomic, readonly) NSTimeInterval lastUpdateDate;

//  Start service
- (void)start;

// Stop service
- (void)stop;

@end
