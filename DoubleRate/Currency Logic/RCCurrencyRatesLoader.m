//
//  RCCurrencyRatesLoader.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RCCurrencyRatesLoader.h"
#import "RCCurrencyRatesParser.h"
#import "RCCurrencyConsts.h"

static const NSTimeInterval kUpdateInterval = 30;
static const NSTimeInterval kLoadingTimeout = 10;

static NSString * const kECBUrl = @"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

NSString * const RCCurrencyRatesUpdatedNotification = @"RCCurrencyRatesUpdatedNotification";
NSString * const RCCurrencyRatesUpdateFailNotification = @"RCCurrencyRatesUpdateFailNotification";

NSString * const kRatesStoreKey = @"rates";
NSString * const kRatesUpdateDateStoreKey = @"ratesUpdateDate";

@interface RCCurrencyRatesLoader()
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSURLSession *loadingSession;
@property (nonatomic, assign) BOOL isRunning;
@property (nonatomic, assign) BOOL isLoading;

// Used to save and restore decimal numbers.
@property (nonatomic, strong) NSLocale *locale;
@end

@implementation RCCurrencyRatesLoader

#pragma mark - Memory Management and Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *conf = [NSURLSessionConfiguration defaultSessionConfiguration];
        conf.timeoutIntervalForRequest = kLoadingTimeout;
        conf.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        conf.allowsCellularAccess = YES;
        self.loadingSession = [NSURLSession sessionWithConfiguration:conf];
        
        self.locale = [NSLocale localeWithLocaleIdentifier:@"en"];
    }
    return self;
}

- (void)dealloc {
    [self unsubscribeNotifications];
}

#pragma mark - Public Interface

+ (RCCurrencyRatesLoader *)sharedInstance {
    static RCCurrencyRatesLoader *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [RCCurrencyRatesLoader new];
    });
    return sharedInstance;
}

- (NSDictionary<NSString *,NSDecimalNumber *> *)rates {
    NSDictionary<NSString *,NSString *> *stringRates = [[NSUserDefaults standardUserDefaults] objectForKey:kRatesStoreKey];
    if (!stringRates) {
        return @{};
    }
    
    //Now convert strings to deciman numbers.
    NSMutableDictionary<NSString *,NSDecimalNumber *> *decimalRates = [NSMutableDictionary new];
    for (NSString *code in stringRates) {
        decimalRates[code] = [NSDecimalNumber decimalNumberWithString:stringRates[code] locale:self.locale];
    }
    
    return decimalRates;
}

- (NSTimeInterval)lastUpdateDate {
    return [[NSUserDefaults standardUserDefaults] doubleForKey:kRatesUpdateDateStoreKey];
}

- (void)start {
    if (self.isRunning) {
        return;
    }
    
    self.isRunning = YES;
    [self subscribeNotifications];
    [self loadRates];
    [self startTimer];
}

- (void)stop {
    if (!self.isRunning) {
        return;
    }
    
    self.isRunning = NO;
    [self unsubscribeNotifications];
}

#pragma mark - Inner Logic

- (void)subscribeNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onApplicationWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)unsubscribeNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Timer Logic

- (void)startTimer {
    NSAssert(self.timer == nil, @"");
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kUpdateInterval
                                                  target:self
                                                selector:@selector(onTimer:)
                                                userInfo:nil repeats:YES];
}

- (void)stopTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)onTimer:(NSTimer *)timer {
    [self loadRates];
}

#pragma mark - Loading Logic

- (void)loadRates {
    if (self.isLoading) {
        return;
    }
    self.isLoading = YES;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    RCCurrencyRatesLoader * __weak weakSelf = self;
    NSURL *url = [NSURL URLWithString:kECBUrl];
    
    NSURLSessionDataTask *task = [self.loadingSession dataTaskWithURL:url
    completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        RCCurrencyRatesLoader *self = weakSelf;
        if (!self) {
            return;
        }

        if (error) {
            [self onLoadingError:error];
        } else {
            NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
            RCCurrencyRatesParser *currencyParser = [RCCurrencyRatesParser new];
            parser.delegate = currencyParser;
            
            BOOL parceResult = [parser parse];
            if (parceResult && [self validateParsingResult:currencyParser.rates]) {
                [self onLoadingSusscessWithResult:currencyParser.rates];
            } else {
                [self onLoadingError:parser.parserError];
            }
        }
    }];
    
    [task resume];
}

- (BOOL)validateParsingResult:(NSDictionary<NSString *, NSDecimalNumber *> *)ratesDict {
    return ratesDict[kGPBCurrencyCode] != nil && ratesDict[kUSDCurrencyCode] != nil;
}

- (void)onLoadingError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self processStopLoading];
        [self notifyUpdateFail];
    });
}

- (void)onLoadingSusscessWithResult:(NSDictionary<NSString *, NSDecimalNumber *> *)ratesDict {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self processStopLoading];
        [self saveLoadingResults:ratesDict];
        [self notifyUpdateSuccess];
    });
}

- (void)processStopLoading {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.isLoading = NO;
}

- (void)saveLoadingResults:(NSDictionary<NSString *, NSDecimalNumber *> *)rates {
    NSAssert(rates.count > 0, @"");
    
    //  Prepare decimal numbers for save
    NSMutableDictionary<NSString *, NSString *> *stringRates = [NSMutableDictionary new];
    for (NSString *code in rates) {
        NSDecimalNumber *decimalValue = rates[code];
        stringRates[code] = [decimalValue descriptionWithLocale:self.locale];
    }
    
    NSTimeInterval time = [[NSDate date] timeIntervalSince1970];
    [[NSUserDefaults standardUserDefaults] setObject:stringRates forKey:kRatesStoreKey];
    [[NSUserDefaults standardUserDefaults] setDouble:time forKey:kRatesUpdateDateStoreKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Notifications Logic

- (void)notifyUpdateSuccess {
    [[NSNotificationCenter defaultCenter] postNotificationName:RCCurrencyRatesUpdatedNotification object:nil];
}

- (void)notifyUpdateFail {
    [[NSNotificationCenter defaultCenter] postNotificationName:RCCurrencyRatesUpdateFailNotification object:nil];
}

#pragma mark - Process App State Notifications

- (void)onApplicationDidEnterBackground:(NSNotification *)notif {
    [self stopTimer];
}

- (void)onApplicationWillEnterForeground:(NSNotification *)notif {
    [self loadRates];
    [self startTimer];
}

@end
