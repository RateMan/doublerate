//
//  RCCurrencyConverter.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

// Implements currency rate calculation logic.
@interface RCCurrencyConverter : NSObject

// Returns how many currency with code 2 we need to buy one currency unit with code 1.
// Returns nil in the absence of information.
- (NSDecimalNumber *)rateForCurrency:(NSString *)code1 relativeCurrency:(NSString *)code2;

// Calculates currency conversion.
// Converts amount of money from source currency to destination currency.
// Returns nil if curency rate not found in system.
- (NSDecimalNumber *)convertAmount:(NSDecimalNumber *)amount
                fromSourceCurrency:(NSString *)code1 toDestinationCurrency:(NSString *)code2;

//  Force convertor realod rates from local storage.
- (void)update;

@end
