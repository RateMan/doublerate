//
//  RCCurencyConsts.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

// These consts represents currency codes.
extern NSString * const kUSDCurrencyCode;
extern NSString * const kGPBCurrencyCode;
extern NSString * const kEURCurrencyCode;
