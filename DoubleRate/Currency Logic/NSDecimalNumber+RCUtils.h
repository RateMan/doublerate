//
//  NSDecimalNumber+RCUtils.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (RCUtils)

- (BOOL)rc_isNotANumber;

- (BOOL)rc_isZero;

- (BOOL)rc_greaterOrEqualThan:(NSDecimalNumber *)number;

- (BOOL)rc_lessOrEqualThan:(NSDecimalNumber *)number;

- (BOOL)rc_greaterThan:(NSDecimalNumber *)number;

- (BOOL)rc_lessThan:(NSDecimalNumber *)number;

@end
