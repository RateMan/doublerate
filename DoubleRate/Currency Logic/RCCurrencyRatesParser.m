//
//  RCCurrencyRatesParser.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCCurrencyRatesParser.h"
#import "RCCurrencyConsts.h"
#import "NSDecimalNumber+RCUtils.h"

static NSString * const kCubeElementName = @"Cube";
static NSString * const kCurrencyAttrName = @"currency";
static NSString * const kRateAttrName = @"rate";

@interface RCCurrencyRatesParser ()
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSDecimalNumber *> *parsingResult;
@end

@implementation RCCurrencyRatesParser


#pragma mark - Public Interface

- (NSDictionary<NSString *,NSDecimalNumber *> *)rates {
    return self.parsingResult ? [self.parsingResult copy] : @{};
}

#pragma mark - NSXMLParserDelegate implementation

- (void)parserDidStartDocument:(NSXMLParser *)parser {
    self.parsingResult = [NSMutableDictionary new];
    self.parsingResult[kEURCurrencyCode] = [NSDecimalNumber one] ; //  Add this for convenient calculations
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName
  namespaceURI:(nullable NSString *)namespaceURI
 qualifiedName:(nullable NSString *)qName
    attributes:(NSDictionary<NSString *, NSString *> *)attributeDict {
    
    //  Here we parse element like this: <Cube currency="DKK" rate="7.4344"/>
    
    if (![elementName isEqualToString:kCubeElementName]) {
        return;
    }
    
    NSString *currencyText = attributeDict[kCurrencyAttrName];
    NSString *rateText = attributeDict[kRateAttrName];
    
    if (currencyText.length == 0 || rateText.length == 0) {
        return;
    }
    
    NSDecimalNumber *rateNumber = [NSDecimalNumber decimalNumberWithString:rateText];
    
    if ([rateNumber rc_isNotANumber] || [rateNumber rc_isZero]) {
        // Here we have parsing error
        [parser abortParsing];
    }
    
    // Ok, collect parsing results
    self.parsingResult[[currencyText uppercaseString]] = rateNumber;
}

@end
