//
//  RCCurrencyUtils.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCCurrencyUtils.h"
#import "RCCurrencyConsts.h"

NSString *signForCurrency(NSString *code) {
    if ([code isEqualToString:kUSDCurrencyCode]) {
        return @"$";
    } else if ([code isEqualToString:kEURCurrencyCode]) {
        return @"€";
    } else if ([code isEqualToString:kGPBCurrencyCode]) {
        return @"£";
    } else {
        return @"";
    }
}


