//
//  RCCurencyConsts.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCCurrencyConsts.h"

NSString * const kUSDCurrencyCode = @"USD";
NSString * const kGPBCurrencyCode = @"GBP";
NSString * const kEURCurrencyCode = @"EUR";
