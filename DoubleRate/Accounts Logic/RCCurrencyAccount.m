//
//  RCCurrencyAccount.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCCurrencyAccount.h"
#import "RCCurrencyConsts.h"
#import "RCAccountManager.h"

@interface RCCurrencyAccount ()
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSDecimalNumber *balance;
@end


@implementation RCCurrencyAccount

#pragma mark - Public Interface

- (void)refresh {
    [[RCAccountManager sharedInstance] refreshAccount:self];
}

#pragma mark - Inner Logic

- (NSString *)description {
    return [NSString stringWithFormat:@"Account: %@ %@", self.currencyCode, self.balance];
}

@end
