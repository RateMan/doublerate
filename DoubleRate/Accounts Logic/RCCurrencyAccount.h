//
//  RCCurrencyAccount.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

//  Represenst users's curreny account.
@interface RCCurrencyAccount : NSObject

// Returns currency code.
@property (nonatomic, readonly) NSString *currencyCode;

// Returns account balance.
@property (nonatomic, readonly) NSDecimalNumber *balance;

// Refresh account info
- (void)refresh;

@end
