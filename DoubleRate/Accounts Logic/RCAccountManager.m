//
//  RCAccountManager.m
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import "RCAccountManager.h"
#import "RCCurrencyAccount.h"
#import "RCCurrencyConverter.h"
#import "RCCurrencyConsts.h"
#import "NSDecimalNumber+RCUtils.h"
#import "RCDecimalNumberBehaviors.h"

NSString * const kRCAccountManagerErrorDomain = @"kRCAccountManagerErrorDomain";

static NSString * kAccountListKey = @"accountList";
static NSInteger kInitialBalance = 100.0;

@interface RCCurrencyAccount (Protected)
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSDecimalNumber *balance;

@end

@interface RCAccountManager ()
// Used to save and restore decimal numbers.
@property (nonatomic, strong) NSLocale *locale;

// Keeps behaviour that we use fo round results.
@property (nonatomic, strong) id<NSDecimalNumberBehaviors> roundBehaviour;

@end

//  Implements account management.
@implementation RCAccountManager

#pragma mark - Memory Management and Initialization

- (instancetype)init {
    self = [super init];
    if (self) {
        self.locale = [NSLocale localeWithLocaleIdentifier:@"en"];
    }
    return self;
}

#pragma mark - Public Interface

+ (RCAccountManager *)sharedInstance {
    static RCAccountManager *sharedInstance = nil;
    static dispatch_once_t onceToken = 0;
    dispatch_once(&onceToken, ^{
        sharedInstance = [RCAccountManager new];
    });
    return sharedInstance;
}

- (RCCurrencyAccount *)accountForCurrencyWithCode:(NSString *)currencyCode {
    NSAssert(currencyCode.length > 0, @"");
    
    NSDictionary *accountDict = [[NSUserDefaults standardUserDefaults] objectForKey:kAccountListKey];
    if (accountDict == nil) {
        return nil;
    }
    
    NSString *balanceString = accountDict[currencyCode];
    if (balanceString == nil) {
        return nil;
    }
    
    NSDecimalNumber *balance = [NSDecimalNumber decimalNumberWithString:balanceString locale:self.locale];
    if ([balance rc_isNotANumber]) {
        return nil;
    }
    
    RCCurrencyAccount *account = [RCCurrencyAccount new];
    account.currencyCode = currencyCode;
    account.balance = balance;
    
    return account;
}

- (void)refreshAccount:(RCCurrencyAccount *)account {
    NSAssert(account != nil, @"");
    RCCurrencyAccount * loadedAccount = [self accountForCurrencyWithCode:account.currencyCode];
    if (!loadedAccount) {
        return;
    }
    
    account.balance = loadedAccount.balance;
}

//  Loads all user accounts.
- (NSArray<RCCurrencyAccount *> *)loadAvailableAccounts {
    NSMutableArray<RCCurrencyAccount *> *accountList = [NSMutableArray new];
    
    NSDictionary<NSString *, NSString *> *accountDict = [[NSUserDefaults standardUserDefaults]
                                                         objectForKey:kAccountListKey];
    for (NSString *code in accountDict) {
        RCCurrencyAccount *account = [RCCurrencyAccount new];
        account.currencyCode = code;
        
        NSDecimalNumber *balance = [NSDecimalNumber decimalNumberWithString:accountDict[code]  locale:self.locale];
        if ([balance rc_isNotANumber]) {
            continue;
        }
        
        account.balance = balance;
        [accountList addObject:account];
    }
    
    return [accountList copy];
}

- (void)setupDefaultAccounts {
    NSDictionary<NSString *, NSString *> *storedList = [[NSUserDefaults standardUserDefaults] objectForKey:kAccountListKey];
    if (storedList.count > 0) {
        return;
    }
    
    NSMutableDictionary<NSString *, NSString *> *accountDict = [NSMutableDictionary new];
    
    NSDecimalNumber *initialValue = [NSDecimalNumber decimalNumberWithDecimal: [@(kInitialBalance) decimalValue]];
    NSString *initialStringBalance = [initialValue descriptionWithLocale:self.locale];
    
    accountDict[kUSDCurrencyCode] = initialStringBalance;
    accountDict[kGPBCurrencyCode] = initialStringBalance;
    accountDict[kEURCurrencyCode] = initialStringBalance;
    
    [[NSUserDefaults standardUserDefaults] setObject:accountDict forKey:kAccountListKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (NSDecimalNumber *)minAmountForExchangeForCurrency:(NSString *)code {
    NSAssert(code != nil, @"");
    //  0.01 for every currency at the current moment
    return [NSDecimalNumber decimalNumberWithMantissa:1 exponent:-2 isNegative:NO];
}

- (void)exchangeFromSource:(RCCurrencyAccount *)sourceAccount
               destination:(RCCurrencyAccount *)destinationAcount
                    amount:(NSDecimalNumber *)amount
                 converter:(RCCurrencyConverter *)converter
                completion:(RCExchangeCompletion)completion {
    
    NSAssert(sourceAccount != nil && destinationAcount != nil, @"");
    NSAssert(converter != nil, @"");
    NSAssert(completion != nil, @"");
    NSAssert(amount != nil, @"");
    
    //
    //  1. Check available funds on source account
    //
    
    NSDecimalNumber *minimalAmount = [self minAmountForExchangeForCurrency:sourceAccount.currencyCode];
    if (![amount rc_greaterOrEqualThan:minimalAmount]) {
        NSError *error = [NSError errorWithDomain:kRCAccountManagerErrorDomain code:RCOperationFailed userInfo:nil];
        completion(NO, error);
        return;
    }
    
    RCCurrencyAccount *updatedSource = [self accountForCurrencyWithCode:sourceAccount.currencyCode];
    if (!updatedSource) {
        NSError *error = [NSError errorWithDomain:kRCAccountManagerErrorDomain code:RCOperationFailed userInfo:nil];
        completion(NO, error);
        return;
    }
    
    if (![updatedSource.balance rc_greaterOrEqualThan:amount]) {
        NSError *error = [NSError errorWithDomain:kRCAccountManagerErrorDomain code:RCInsufficientFundsError userInfo:nil];
        completion(NO, error);
        return;
    }
    
    //
    //  2. Load most resent rates from local storage
    //
    
    [converter update];
    
    
    //
    //  3. Calculate destination ammount
    //
    
    NSDecimalNumber *destinationAmount = [converter convertAmount:amount
                                               fromSourceCurrency:sourceAccount.currencyCode
                                            toDestinationCurrency:destinationAcount.currencyCode];
    
    if (destinationAmount == nil) {
        NSError *error = [NSError errorWithDomain:kRCAccountManagerErrorDomain
                                             code:RCOperationFailed userInfo:nil];
        completion(NO, error);
        return;
    }
    
    //
    //  4. Update balance of each account
    //
    
    RCCurrencyAccount *updatedDestination = [self accountForCurrencyWithCode:destinationAcount.currencyCode];
    if (!updatedDestination) {
        NSError *error = [NSError errorWithDomain:kRCAccountManagerErrorDomain code:RCOperationFailed userInfo:nil];
        completion(NO, error);
        return;
    }
    
    NSMutableDictionary<NSString *, NSString *> *accountDict = [NSMutableDictionary new];
    NSDecimalNumber *newSourceBalance = [updatedSource.balance decimalNumberBySubtracting:amount];
    NSDecimalNumber *newTargetBalance = [updatedDestination.balance decimalNumberByAdding:destinationAmount];
    
    //  Round each value
    RCBalanceRoundBehaviour *roundBehaviour = [RCBalanceRoundBehaviour new];
    newSourceBalance = [newSourceBalance decimalNumberByRoundingAccordingToBehavior:roundBehaviour];
    newTargetBalance = [newTargetBalance decimalNumberByRoundingAccordingToBehavior:roundBehaviour];
    
    accountDict[sourceAccount.currencyCode] = [newSourceBalance descriptionWithLocale:self.locale];
    accountDict[destinationAcount.currencyCode] = [newTargetBalance descriptionWithLocale:self.locale];;
    
    //
    //  5. Update accounts
    //
    
    [self updateAccountsWithDictionary:accountDict];
    completion(YES, nil);
}

#pragma mark - Inner Logic

- (void)updateAccountsWithDictionary:(NSDictionary<NSString *, NSString *> *)accountDict {
    NSAssert(accountDict != nil, @"");
    
    NSDictionary *prevDict = [[NSUserDefaults standardUserDefaults] objectForKey:kAccountListKey];
    if (!prevDict) {
        prevDict = @{};
    }
    NSMutableDictionary *resultDict = [NSMutableDictionary dictionaryWithDictionary:prevDict];
    [resultDict addEntriesFromDictionary:accountDict];
    
    [[NSUserDefaults standardUserDefaults] setObject:resultDict forKey:kAccountListKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
