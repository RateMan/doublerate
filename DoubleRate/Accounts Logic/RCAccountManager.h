//
//  RCAccountManager.h
//  Double Rate
//
//  Created by Dmitry on 11.02.17.
//  Copyright © 2017 Dmitry. All rights reserved.
//

#import <Foundation/Foundation.h>

@class  RCCurrencyConverter, RCCurrencyAccount;

//  Error domain for account manager operations.
extern NSString * const kRCAccountManagerErrorDomain;

// Error codes for kRCAccountManagerErrorDomain.
typedef NS_ENUM(NSInteger, RCAccountManagerErrorCode) {
    
    // Insufficient funds for operation
    RCInsufficientFundsError,
    
    // Try to exchange to little amount of currency.
    RCTooLittleAmountForExchange,
    
    // Operation failed
    RCOperationFailed
};

typedef void (^RCExchangeCompletion)(BOOL success, NSError *error);

// Manages accounts
@interface RCAccountManager : NSObject

// Returnds shared instance.
+ (RCAccountManager *)sharedInstance;

// Min amout of money user can convert.
- (NSDecimalNumber *)minAmountForExchangeForCurrency:(NSString *)code;

// Loads account for specified currency code.
// Returns nil if account does not exist.
- (RCCurrencyAccount *)accountForCurrencyWithCode:(NSString *)currencyCode;

// Refresh acount balance and so on.
- (void)refreshAccount:(RCCurrencyAccount *)account;

//  Loads all user accounts.
- (NSArray<RCCurrencyAccount *> *)loadAvailableAccounts;

//  Creates default accounts for user.
//  USD, GPB, EUR. Each has balance equal 100.
- (void)setupDefaultAccounts;

// Makes currency exchange.
// Converts specified amount of money from source account and puts them on destination account.
// Completion will be called on main thread.
- (void)exchangeFromSource:(RCCurrencyAccount *)sourceAccount
               destination:(RCCurrencyAccount *)destinationAcount
                    amount:(NSDecimalNumber *)amount
                 converter:(RCCurrencyConverter *)converter
                completion:(RCExchangeCompletion)completion;


@end
